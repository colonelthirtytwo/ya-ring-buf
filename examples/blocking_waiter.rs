//! Example of a ring buffer with a blocking waiter on both sides.

use std::thread;

use ya_ring_buf::{
	ClosedError,
	PushError,
};

fn main() {
	// Create buffer
	let (mut sender, mut receiver) = ya_ring_buf::new_blocking(3);

	// Start up threads
	let sender_thread = thread::spawn(move || {
		for i in 0.. {
			// Wait for a chance to send.
			// For a BlockingWaiter, this just blocks. For an AsyncWaiter, this returns a future to `await`.
			// For your own custom waiter, it can do whatever. Maybe signal an `mio` `Waker`!
			sender.wait();

			// Attempt sending
			match sender.push(i) {
				Ok(()) => {
					// Sent ok!
				}
				Err(PushError::Closed(v)) => {
					// Other end closed, we can exit.
					assert_eq!(v, i);
					return;
				}
				Err(PushError::Full(_)) => {
					// Should not happen. wait should not return until we have space.
					unreachable!()
				}
			}
		}
	});

	let receiver_thread = thread::spawn(move || {
		for i in 0..10 {
			// Wait for an element to arrive
			receiver.wait();
			match receiver.pop() {
				Ok(Some(v)) => {
					// Got an element, check it
					assert_eq!(v, i);
				}
				Err(ClosedError(())) => {
					// The sender was dropped
					return;
				}
				Ok(None) => {
					// Should not happen. wait should not return until we have an item.
					unreachable!()
				}
			}
		}
	});

	sender_thread.join().unwrap();
	receiver_thread.join().unwrap();
}
