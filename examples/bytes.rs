use std::{
	io::{
		Cursor,
		Read,
		Write,
	},
	thread,
};

use ya_ring_buf::ClosedError;

fn main() {
	// Create some standin files:
	let mut file_in = std::io::Cursor::new(CONTENTS);
	let mut file_out = Cursor::new(vec![0u8; 0]);

	// Create our "pipe":
	let (mut sender, mut receiver) = ya_ring_buf::new_waitless::<u8>(2056);

	// Producer thread
	let t1 = thread::spawn(move || {
		loop {
			// Get a view into the writable section of the buffer.
			//
			// This is a wrapper structure over two slices, which together make up the portion
			// of the buffer that is available for writing.
			//
			// With this, we can directly `read` into the buffer.
			let mut view = sender.view().expect("The sender was dropped!");
			if view.is_empty() {
				// No space available. Come back later.
				// A real application should block here.
				thread::yield_now();
			}

			// Fill first part of the buffer
			let read_1_amnt = file_in.read(view.0).unwrap();
			if read_1_amnt == 0 {
				// Done!
				break;
			}
			if read_1_amnt < view.0.len() {
				// Ran out of bytes. Produce what we have.
				view.produce(read_1_amnt);
				continue;
			}

			// Fill in second part of the buffer
			let read_2_amnt = file_in.read(view.1).unwrap();
			if read_2_amnt == 0 {
				// Done!
				view.produce(read_1_amnt);
				break;
			}
			// Finished reading. Produce the amount of bytes we read.
			view.produce(read_1_amnt + read_2_amnt);

			// You could also be lazy and fill just the first buffer in each loop - the
			// remaining capacity will be returned on the next iteration.

			// If you're really fancy, you can use `as_slices_uninitialized` to skip the
			// default initialization and get slices of `MaybeUninit<u8>` to read into.
		}

		// Finished writing. The receiver will be dropped, signalling the consumer thread
		// to exit.
	});

	// Consumer thread
	let t2 = thread::spawn(move || {
		loop {
			// Grab a view into the readable section of the buffer.
			//
			// Very similar to the sender's view, but instead of writing,
			// we read.
			//
			// If the buffer is empty and the sender is dropped, we get a
			// `CloseError` instead, meaning its time to stop.
			let view = match receiver.view() {
				Ok(view) => view,
				Err(ClosedError(())) => break,
			};

			if view.is_empty() {
				// Empty buffer. Come back later.
				// A real application should block here.
				thread::yield_now();
			}

			// Write the contents
			file_out.write_all(view.0).unwrap();
			file_out.write_all(view.1).unwrap();
		}

		// Check that we got everything.
		assert_eq!(file_out.into_inner().as_slice(), CONTENTS);
	});

	t1.join().unwrap();
	t2.join().unwrap();
}

/// Some data to play with.
const CONTENTS: &[u8] = br#"
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Turpis egestas maecenas pharetra convallis posuere morbi leo urna. Et leo duis ut diam quam nulla. Interdum velit laoreet
id donec ultrices tincidunt arcu non sodales. Faucibus turpis in eu mi bibendum. Velit egestas dui id ornare arcu odio ut
sem. Varius quam quisque id diam vel. Viverra tellus in hac habitasse. Id aliquet risus feugiat in ante metus. Mauris in
aliquam sem fringilla ut morbi tincidunt. Turpis egestas maecenas pharetra convallis posuere. Aliquet eget sit amet tellus
cras adipiscing. Fames ac turpis egestas sed. Pulvinar etiam non quam lacus suspendisse. Eu scelerisque felis imperdiet
proin. Ultricies tristique nulla aliquet enim tortor at. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Sit
amet risus nullam eget felis eget nunc lobortis.

Dictumst quisque sagittis purus sit amet volutpat. Semper feugiat nibh sed pulvinar proin. Pellentesque habitant morbi
tristique senectus et netus et malesuada fames. Id diam maecenas ultricies mi eget. Non blandit massa enim nec dui nunc
mattis enim. A diam sollicitudin tempor id eu nisl nunc mi ipsum. Quis blandit turpis cursus in hac habitasse platea.
Vitae tempus quam pellentesque nec nam. Luctus accumsan tortor posuere ac ut consequat semper viverra nam. Magna sit amet
purus gravida quis blandit turpis cursus. Proin nibh nisl condimentum id.

Mattis enim ut tellus elementum sagittis vitae et leo duis. Fermentum dui faucibus in ornare quam viverra orci sagittis eu.
Viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. At imperdiet dui accumsan sit amet nulla facilisi. Aliquam
sem fringilla ut morbi tincidunt augue interdum velit. Consequat nisl vel pretium lectus quam id leo in vitae. Felis donec
et odio pellentesque diam volutpat commodo. Non pulvinar neque laoreet suspendisse interdum. Commodo nulla facilisi nullam
vehicula ipsum a arcu cursus. Vulputate sapien nec sagittis aliquam. Dignissim suspendisse in est ante in nibh. Magnis dis
parturient montes nascetur ridiculus mus.

Et malesuada fames ac turpis egestas maecenas pharetra convallis. Metus aliquam eleifend mi in nulla posuere sollicitudin
aliquam ultrices. Sit amet est placerat in egestas erat imperdiet sed euismod. Integer feugiat scelerisque varius morbi
enim nunc faucibus a. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Ac odio tempor orci dapibus ultrices
in iaculis nunc sed. Vitae nunc sed velit dignissim. A diam sollicitudin tempor id. Quis ipsum suspendisse ultrices gravida
dictum. At tellus at urna condimentum. Dignissim cras tincidunt lobortis feugiat vivamus at augue eget arcu. Ipsum
suspendisse ultrices gravida dictum fusce. A scelerisque purus semper eget duis at. Risus ultricies tristique nulla aliquet
enim. Elementum nibh tellus molestie nunc non blandit massa enim. Quisque non tellus orci ac auctor augue mauris augue. Morbi
tempus iaculis urna id volutpat lacus laoreet non. Mauris ultrices eros in cursus turpis massa.

Facilisi etiam dignissim diam quis enim. Parturient montes nascetur ridiculus mus. Tristique et egestas quis ipsum. Quis
commodo odio aenean sed adipiscing diam donec. Sit amet consectetur adipiscing elit. Viverra justo nec ultrices dui sapien
eget mi. Dis parturient montes nascetur ridiculus mus mauris vitae. Congue mauris rhoncus aenean vel elit scelerisque mauris.
Id neque aliquam vestibulum morbi blandit. Ullamcorper morbi tincidunt ornare massa eget egestas purus viverra. Volutpat sed
cras ornare arcu dui vivamus arcu felis. Tincidunt dui ut ornare lectus. Dolor sit amet consectetur adipiscing elit pellentesque
habitant. Vel orci porta non pulvinar neque laoreet suspendisse. Cras pulvinar mattis nunc sed blandit. Venenatis tellus in
metus vulputate eu scelerisque felis imperdiet. Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus est.
Venenatis a condimentum vitae sapien pellentesque habitant morbi. Dignissim enim sit amet venenatis.
"#;
