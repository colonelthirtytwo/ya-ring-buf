//! Waiting strategies: abstraction over blocking and futures.

use core::sync::atomic::{
	AtomicUsize,
	Ordering,
};

mod seal {
	pub trait Sealed {}
}

/// Structure passed to a waiter to test whether wakeup should happen.
#[derive(Debug)]
pub struct WakeCheck<'a> {
	pub(crate) head: &'a AtomicUsize,
	pub(crate) tail: &'a AtomicUsize,
	pub(crate) closed_count: &'a AtomicUsize,
	pub(crate) cap: usize,
	pub(crate) is_producer: bool,
}
impl<'a> WakeCheck<'a> {
	/// Checks if wakeup should happen.
	pub fn should_wake(&self) -> bool {
		if self.closed_count.load(Ordering::Acquire) > 0 {
			return true;
		}

		let (head, tail) = (
			self.head.load(Ordering::Acquire),
			self.tail.load(Ordering::Acquire),
		);
		debug_assert!(head < self.cap);
		debug_assert!(tail < self.cap);

		if self.is_producer {
			let limit = head.checked_sub(1).unwrap_or(self.cap - 1);
			tail != limit
		} else {
			head != tail
		}
	}
}

/// Implementation of a waiting strategy.
///
/// An abstraction over synchronous blocking or asynchronous futures, depending on the
/// desired use case.
pub trait Waiter<'a> {
	/// Return value of `wait`.
	///
	/// The exact type and semantics depend on the waiter, but generally, waiters
	/// that block should return `()` while async waiters should return a `Future`
	/// (hence the name).
	type Future: 'a;

	/// Begins waiting.
	///
	/// Takes a [`WakeCheck`] object that may be used to see if
	/// the buffer has space (or the opposite end has closed).
	///
	/// May either block or return a future.
	fn wait(&'a self, checker: WakeCheck<'a>) -> Self::Future;

	/// Notifies that waiters should wake and re-check the `WakeCheck` it received.
	///
	/// This may be called while the stack is unwinding - for best results, this should
	/// not panic.
	fn notify(&self);
}

/// Implementation of a waiting strategy.
///
/// Super-trait of [`Waiter`] - check the documentation for that instead. This exists so that
/// the buffer can opt-out of waiters by using `()` as a waiter.
pub trait OptionalWaiter: seal::Sealed {
	fn notify(&self);
}
impl<T: for<'a> Waiter<'a>> seal::Sealed for T {}
impl<T: for<'a> Waiter<'a>> OptionalWaiter for T {
	fn notify(&self) {
		<Self as Waiter>::notify(self)
	}
}

impl seal::Sealed for () {}
impl OptionalWaiter for () {
	fn notify(&self) {}
}

#[cfg(feature = "std")]
mod sync_waiter {
	#[cfg(not(loom))]
	use std::sync::{
		atomic::{
			AtomicBool,
			Ordering,
		},
		Condvar,
		Mutex,
	};

	#[cfg(loom)]
	use loom::{
		atomic::{
			AtomicBool,
			Ordering,
		},
		Condvar,
		Mutex,
	};

	use super::*;

	/// Blocking waiter.
	pub struct BlockingWaiter {
		flag: AtomicBool,
		mutex: Mutex<()>,
		cond: Condvar,
	}
	impl BlockingWaiter {
		pub fn new() -> Self {
			Self {
				flag: AtomicBool::new(false),
				mutex: Mutex::new(()),
				cond: Condvar::new(),
			}
		}
	}
	impl<'a> Waiter<'a> for BlockingWaiter {
		type Future = ();

		fn wait(&'a self, checker: WakeCheck<'a>) -> Self::Future {
			while !checker.should_wake() {
				if self.flag.swap(false, Ordering::Acquire) {
					continue;
				}
				let m = self.mutex.lock().unwrap();
				let _ = self
					.cond
					.wait_while(m, |_| !self.flag.swap(false, Ordering::Acquire))
					.unwrap();
			}
		}

		fn notify(&self) {
			let _ = self.mutex.lock().unwrap();
			self.flag.store(true, Ordering::Relaxed);
			self.cond.notify_all();
		}
	}
	impl Default for BlockingWaiter {
		fn default() -> Self {
			Self::new()
		}
	}
}

#[cfg(feature = "std")]
pub use sync_waiter::*;

#[cfg(feature = "async")]
mod async_waiter {
	use atomic_waker::AtomicWaker;

	use super::*;

	/// Future returned by the [`AsyncWaiter`].
	#[must_use]
	pub struct ReadyFuture<'a>(&'a AsyncWaiter, WakeCheck<'a>);
	impl<'a> core::future::Future for ReadyFuture<'a> {
		type Output = ();

		fn poll(
			self: core::pin::Pin<&mut Self>,
			cx: &mut core::task::Context<'_>,
		) -> core::task::Poll<Self::Output> {
			if self.1.should_wake() {
				return core::task::Poll::Ready(());
			}

			self.0.waker.register(cx.waker());
			if self.1.should_wake() {
				core::task::Poll::Ready(())
			} else {
				core::task::Poll::Pending
			}
		}
	}

	/// Asynchronous waiter.
	///
	/// Returns a future that resolves when ready.
	///
	/// Don't have more than one task waiting on the future at a time.
	pub struct AsyncWaiter {
		waker: AtomicWaker,
	}
	impl AsyncWaiter {
		pub fn new() -> Self {
			Self {
				waker: AtomicWaker::new(),
			}
		}
	}
	impl<'a> Waiter<'a> for AsyncWaiter {
		type Future = ReadyFuture<'a>;

		fn wait(&'a self, checker: WakeCheck<'a>) -> Self::Future {
			ReadyFuture(self, checker)
		}

		fn notify(&self) {
			self.waker.wake();
		}
	}
	impl Default for AsyncWaiter {
		fn default() -> Self {
			Self::new()
		}
	}
}
#[cfg(feature = "async")]
pub use async_waiter::*;
