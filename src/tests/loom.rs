// Most of these busy loop, which isn't great design, but sufficient for tests.
#[test]
fn push_pop_one() {
	loom::model(|| {
		let (mut send, mut recv) = new::<i32>(4);
		let send_handle = loom::thread::spawn(move || {
			for i in 0..9 {
				match send.push(i) {
					Some(v) => {
						assert_eq!(v, i);
						break;
					}
					None => {
						loom::thread::yield_now();
					}
				}
			}
		});
		let recv_handle = loom::thread::spawn(move || {
			for i in 0..9 {
				if let Some(v) = recv.pop() {
					assert_eq!(v, i);
				} else {
					break;
				}
			}
		});
		send_handle.join().unwrap();
		recv_handle.join().unwrap();
	});
}
