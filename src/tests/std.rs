use std::{
	thread,
	time::{
		Duration,
	},
};
#[cfg(not(miri))]
use std::time::Instant;

use crate::{
	new_blocking,
	ClosedError,
	PushError,
};

// Stub out some stuff on miri since it doesn't support the underlying syscall.
// Note that sleep requires `MIRIFLAGS="-Zmiri-disable-isolation"`.
#[cfg(miri)]
struct Instant;
#[cfg(miri)]
impl Instant {
	pub fn now() -> Self { Self }
	pub fn duration_since(&self, _: Self) -> Duration {
		Duration::from_secs(999)
	}
}

#[test]
#[cfg_attr(all(miri, windows), ignore)]
fn receiver_wait() {
	let (mut sender, mut receiver) = new_blocking(15);

	let start_wait = Instant::now();
	let t = thread::spawn(move || {
		receiver.wait();
		assert_eq!(receiver.pop(), Ok(Some(1)));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	thread::sleep(Duration::from_millis(100));
	sender.push(1).unwrap();

	t.join().unwrap();
}

#[test]
#[cfg_attr(all(miri, windows), ignore)]
fn sender_wait() {
	let (mut sender, mut receiver) = new_blocking(15);

	sender.extend(0..).unwrap();

	let start_wait = Instant::now();
	let t = thread::spawn(move || {
		sender.wait();
		sender.push(1).unwrap();
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	thread::sleep(Duration::from_millis(100));
	assert_eq!(receiver.pop(), Ok(Some(0)));
	t.join().unwrap();
}

#[test]
#[cfg_attr(all(miri, windows), ignore)]
fn receiver_wait_view() {
	let (mut sender, mut receiver) = new_blocking(15);

	let start_wait = Instant::now();
	let t = thread::spawn(move || {
		receiver.wait();
		assert_eq!(receiver.pop(), Ok(Some(123)));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	thread::sleep(Duration::from_millis(100));
	let mut view = sender.view().unwrap();
	view.0[0] = 123;
	view.produce(1);

	t.join().unwrap();
}

#[test]
#[cfg_attr(all(miri, windows), ignore)]
fn sender_wait_view() {
	let (mut sender, mut receiver) = new_blocking(15);

	sender.extend(0..).unwrap();

	let start_wait = Instant::now();
	let t = thread::spawn(move || {
		sender.wait();
		sender.push(1).unwrap();
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	thread::sleep(Duration::from_millis(100));
	let mut view = receiver.view().unwrap();
	assert!(view.iter().map(|v| *v).eq(0..15));
	view.consume_all();
	t.join().unwrap();
}

#[test]
#[cfg_attr(all(miri, windows), ignore)]
fn receiver_wait_closed() {
	let (sender, mut receiver) = new_blocking::<i32>(15);

	let start_wait = Instant::now();
	let t = thread::spawn(move || {
		receiver.wait();
		assert_eq!(receiver.pop(), Err(ClosedError(())));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	thread::sleep(Duration::from_millis(100));
	std::mem::drop(sender);

	t.join().unwrap();
}

#[test]
#[cfg_attr(all(miri, windows), ignore)]
fn sender_wait_closed() {
	let (mut sender, receiver) = new_blocking(15);

	sender.extend(0..).unwrap();

	let start_wait = Instant::now();
	let t = thread::spawn(move || {
		sender.wait();
		assert_eq!(sender.push(1), Err(PushError::Closed(1)));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	thread::sleep(Duration::from_millis(100));
	std::mem::drop(receiver);
	t.join().unwrap();
}
