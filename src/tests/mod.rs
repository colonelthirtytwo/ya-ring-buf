#[cfg(not(loom))]
pub mod core;

#[cfg(loom)]
pub mod loom;

#[cfg(feature = "std")]
pub mod std;

#[cfg(all(feature = "async", feature = "std", not(all(miri, windows))))]
pub mod r#async;
