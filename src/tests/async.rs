use std::time::{
	Duration,
	Instant,
};

use crate::{
	new_async,
	ClosedError,
	PushError,
};

#[tokio::test]
async fn receiver_wait() {
	let (mut sender, mut receiver) = new_async(15);

	let start_wait = Instant::now();
	let t = tokio::task::spawn(async move {
		receiver.wait().await;
		assert_eq!(receiver.pop(), Ok(Some(1)));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	tokio::time::sleep(Duration::from_millis(100)).await;
	sender.push(1).unwrap();

	t.await.unwrap()
}

#[tokio::test]
async fn sender_wait() {
	let (mut sender, mut receiver) = new_async(15);

	sender.extend(0..).unwrap();

	let start_wait = Instant::now();
	let t = tokio::task::spawn(async move {
		sender.wait().await;
		sender.push(1).unwrap();
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	tokio::time::sleep(Duration::from_millis(100)).await;
	assert_eq!(receiver.pop(), Ok(Some(0)));
	t.await.unwrap();
}

#[tokio::test]
async fn receiver_wait_view() {
	let (mut sender, mut receiver) = new_async(15);

	let start_wait = Instant::now();
	let t = tokio::task::spawn(async move {
		receiver.wait().await;
		assert_eq!(receiver.pop(), Ok(Some(123)));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	tokio::time::sleep(Duration::from_millis(100)).await;
	let mut view = sender.view().unwrap();
	view.0[0] = 123;
	view.produce(1);

	t.await.unwrap();
}

#[tokio::test]
async fn sender_wait_view() {
	let (mut sender, mut receiver) = new_async(15);

	sender.extend(0..).unwrap();

	let start_wait = Instant::now();
	let t = tokio::task::spawn(async move {
		sender.wait().await;
		sender.push(1).unwrap();
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	tokio::time::sleep(Duration::from_millis(100)).await;
	let mut view = receiver.view().unwrap();
	assert!(view.iter().map(|v| *v).eq(0..15));
	view.consume_all();
	t.await.unwrap();
}

#[tokio::test]
async fn receiver_wait_closed() {
	let (sender, mut receiver) = new_async::<i32>(15);

	let t = tokio::task::spawn(async move {
		let start_wait = Instant::now();
		receiver.wait().await;
		assert_eq!(receiver.pop(), Err(ClosedError(())));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	tokio::time::sleep(Duration::from_millis(100)).await;
	std::mem::drop(sender);

	t.await.unwrap();
}

#[tokio::test]
async fn sender_wait_closed() {
	let (mut sender, receiver) = new_async(15);

	sender.extend(0..).unwrap();

	let start_wait = Instant::now();
	let t = tokio::task::spawn(async move {
		sender.wait().await;
		assert_eq!(sender.push(1), Err(PushError::Closed(1)));
		assert!(Instant::now().duration_since(start_wait) >= Duration::from_millis(100));
	});

	tokio::time::sleep(Duration::from_millis(100)).await;
	std::mem::drop(receiver);
	t.await.unwrap();
}
