use alloc::{
	sync::Arc,
	vec::Vec,
};
use std::sync::atomic::{
	AtomicUsize,
	Ordering,
};

use crate::{
	new_waitless as new,
	waiter::WakeCheck,
	ClosedError,
	PushError,
};

#[test]
fn push_pop_one() {
	let (mut send, mut recv) = new(10);
	for i in 0..40 {
		assert_eq!(send.push(i), Ok(()));
		assert_eq!(recv.pop(), Ok(Some(i)));
	}
	assert_eq!(recv.pop(), Ok(None));
}

#[test]
fn pop_empty() {
	let (_send, mut recv) = new::<i32>(10);
	assert_eq!(recv.pop(), Ok(None));
}

#[test]
fn push_to_capacity() {
	let (mut send, mut recv) = new(10);
	for _ in 0..4 {
		for j in 0..10 {
			assert_eq!(send.push(j), Ok(()));
		}
		assert_eq!(send.push(10), Err(PushError::Full(10)));

		for j in 0..10 {
			assert_eq!(recv.pop(), Ok(Some(j)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
#[should_panic]
fn zero_capacity() {
	new::<i32>(0);
}

#[test]
fn zst_push_pop_one() {
	let (mut send, mut recv) = new::<()>(10);
	for _ in 0..40 {
		assert_eq!(send.push(()), Ok(()));
		assert_eq!(recv.pop(), Ok(Some(())));
	}
	assert_eq!(recv.pop(), Ok(None));
}

#[test]
fn extend() {
	let (mut send, mut recv) = new(10);
	for _ in 0..4 {
		assert_eq!(send.extend(0..8).unwrap().next(), None);
		for j in 0..8 {
			assert_eq!(recv.pop(), Ok(Some(j)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn extend_to_capacity() {
	let (mut send, mut recv) = new(10);
	for _ in 0..4 {
		let mut remaining = send.extend(0..11).unwrap();
		assert_eq!(remaining.next(), Some(10));
		assert!(remaining.next().is_none());
		for j in 0..10 {
			assert_eq!(recv.pop(), Ok(Some(j)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn push_slice() {
	let (mut send, mut recv) = new(15);
	for _ in 0..4 {
		let vec = (0..10).collect::<Vec<_>>();
		assert_eq!(send.push_slice(&vec), Ok(10));
		for i in 0..10 {
			assert_eq!(recv.pop(), Ok(Some(i)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn read_view() {
	let (mut send, mut recv) = new(15);
	for _ in 0..4 {
		let expected = (0..10).collect::<Vec<_>>();
		assert_eq!(send.extend(expected.iter().copied()).unwrap().next(), None);
		let slices = recv.view().unwrap();
		assert_eq!(slices.0.len() + slices.1.len(), 10);
		let vals = slices
			.0
			.iter()
			.chain(slices.1.iter())
			.copied()
			.collect::<Vec<_>>();
		assert_eq!(vals, expected);
		slices.consume_all();
	}
}

#[test]
fn read_view_no_consume() {
	let (mut send, mut recv) = new(15);
	for _ in 0..4 {
		let expected = (0..10).collect::<Vec<_>>();
		assert_eq!(send.extend(expected.iter().copied()).unwrap().next(), None);
		let slices = recv.view().unwrap();
		assert_eq!(slices.0.len() + slices.1.len(), 10);
		let vals = slices
			.0
			.iter()
			.chain(slices.1.iter())
			.copied()
			.collect::<Vec<_>>();
		assert_eq!(vals, expected);

		for v in expected {
			assert_eq!(recv.pop(), Ok(Some(v)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn read_view_partial_consume() {
	let (mut send, mut recv) = new(15);
	for _ in 0..4 {
		let expected = (0..10).collect::<Vec<_>>();
		assert_eq!(send.extend(expected.iter().copied()).unwrap().next(), None);
		let slices = recv.view().unwrap();
		assert_eq!(slices.0.len() + slices.1.len(), 10);
		let vals = slices
			.0
			.iter()
			.chain(slices.1.iter())
			.copied()
			.collect::<Vec<_>>();
		assert_eq!(vals, expected);

		slices.consume(5);

		for v in &expected[5..] {
			assert_eq!(recv.pop(), Ok(Some(*v)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn read_view_empty() {
	let (_send, mut recv) = new::<i32>(15);
	let slices = recv.view().unwrap();
	assert_eq!(slices.0, &[]);
	assert_eq!(slices.1, &[]);
}

#[test]
fn contents_refcounted() {
	let (mut send, mut recv) = new(10);
	let v = Arc::new(());
	for _ in 0..4 {
		for _ in 0..10 {
			assert!(send.push(v.clone()).is_ok());
		}
		assert!(send.push(v.clone()).is_err());
		assert_eq!(Arc::strong_count(&v), 11);

		for _ in 0..10 {
			assert!(recv.pop().unwrap().is_some());
		}
		assert_eq!(recv.pop(), Ok(None));
		assert_eq!(Arc::strong_count(&v), 1);
	}
}

#[test]
fn contents_refcounted_read_view_drop() {
	let (mut send, mut recv) = new(10);
	let v = Arc::new(());
	for _ in 0..4 {
		for _ in 0..10 {
			assert!(send.push(v.clone()).is_ok());
		}
		assert!(send.push(v.clone()).is_err());
		assert_eq!(Arc::strong_count(&v), 11);

		recv.view().unwrap().consume_all();
		assert_eq!(recv.pop(), Ok(None));
		assert_eq!(Arc::strong_count(&v), 1);
	}
}

#[test]
fn read_view_while_pushing() {
	let (mut send, mut recv) = new(15);
	for _ in 0..4 {
		assert!(send.extend(0..5).unwrap().next().is_none());

		let slices = recv.view().unwrap();
		assert_eq!(slices.0.len() + slices.1.len(), 5);
		assert!(slices.0.iter().chain(slices.1.iter()).copied().eq(0..5));

		assert!(send.extend(5..15).unwrap().next().is_none());
		assert_eq!(send.push(15), Err(PushError::Full(15)));

		assert_eq!(slices.0.len() + slices.1.len(), 5);
		assert!(slices.0.iter().chain(slices.1.iter()).copied().eq(0..5));
		slices.consume_all();

		for i in 5..15 {
			assert_eq!(recv.pop(), Ok(Some(i)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn close_send() {
	let (send, mut recv) = new::<i32>(15);
	core::mem::drop(send);
	assert_eq!(recv.pop(), Err(ClosedError(())));
}

#[test]
fn close_recv() {
	let (mut send, recv) = new::<i32>(15);
	core::mem::drop(recv);
	assert_eq!(send.push(123), Err(PushError::Closed(123)));
}

#[test]
fn close_send_with_data() {
	let (mut send, mut recv) = new::<i32>(15);
	send.push(123).unwrap();
	core::mem::drop(send);
	assert_eq!(recv.pop(), Ok(Some(123)));
	assert_eq!(recv.pop(), Err(ClosedError(())));
}

#[test]
fn write_view() {
	let (mut send, mut recv) = new::<i32>(15);
	let test_vec = (0..10).collect::<Vec<_>>();
	for _ in 0..4 {
		let mut view = send.view().unwrap();
		let full_len = view.0.len() + view.1.len();
		assert_eq!(full_len, 15, "{:?}", view.shared);

		let l1 = test_vec.len().min(view.0.len());
		let l2 = test_vec.len() - l1;
		view.0[0..l1].copy_from_slice(&test_vec[0..l1]);
		view.1[0..l2].copy_from_slice(&test_vec[l1..]);

		view.produce(test_vec.len());

		assert_eq!(recv.available_len(), test_vec.len());
		for i in test_vec.iter().copied() {
			assert_eq!(recv.pop(), Ok(Some(i)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn uninitialized_write_view() {
	let (mut send, mut recv) = new::<i32>(15);
	let test_vec = (0..10).collect::<Vec<_>>();
	for _ in 0..4 {
		let mut view = send.uninitialized_view().unwrap();
		let full_len = view.0.len() + view.1.len();
		assert_eq!(full_len, 15, "{:?}", view.shared);

		for (i, slot) in view.iter().enumerate() {
			if let Some(v) = test_vec.get(i) {
				slot.write(*v);
			}
		}

		unsafe {
			view.produce(test_vec.len());
		}

		assert_eq!(recv.available_len(), test_vec.len());
		for i in test_vec.iter().copied() {
			assert_eq!(recv.pop(), Ok(Some(i)));
		}
		assert_eq!(recv.pop(), Ok(None));
	}
}

#[test]
fn write_view_drop_unproduced() {
	let (mut send, mut recv) = new::<Arc<()>>(15);
	let test_val = Arc::new(());
	for _ in 0..4 {
		assert_eq!(send.available_len(), 15);
		let mut view = send.view().unwrap();
		for slot in view.iter() {
			*slot = Arc::clone(&test_val);
		}
		assert_eq!(Arc::strong_count(&test_val), 16);

		view.produce(5);
		assert_eq!(send.available_len(), 10);
		assert_eq!(Arc::strong_count(&test_val), 6);

		assert_eq!(recv.available_len(), 5);
		for _ in 0..5 {
			assert!(matches!(recv.pop(), Ok(Some(_))));
		}
		assert_eq!(recv.pop(), Ok(None));
		assert_eq!(recv.available_len(), 0);
	}
}

#[test]
fn write_sized_view_drop_unproduced() {
	let (mut send, mut recv) = new::<Arc<()>>(15);
	let test_val = Arc::new(());
	for _ in 0..4 {
		assert_eq!(send.available_len(), 15);
		let mut view = send.sized_view(5).unwrap();
		for slot in view.iter() {
			*slot = Arc::clone(&test_val);
		}
		assert_eq!(Arc::strong_count(&test_val), 6);

		view.produce(3);
		assert_eq!(send.available_len(), 12);
		assert_eq!(Arc::strong_count(&test_val), 4);

		assert_eq!(recv.available_len(), 3);
		for _ in 0..3 {
			assert!(matches!(recv.pop(), Ok(Some(_))));
		}
		assert_eq!(recv.pop(), Ok(None));
		assert_eq!(recv.available_len(), 0);
	}
}

#[test]
fn drop_contents() {
	let (mut send, recv) = new::<Arc<()>>(15);
	let test_val = Arc::new(());
	for _ in 0..10 {
		send.push(Arc::clone(&test_val)).unwrap();
	}
	assert_eq!(Arc::strong_count(&test_val), 11);
	std::mem::drop(send);
	std::mem::drop(recv);

	assert_eq!(Arc::strong_count(&test_val), 1);
}

#[test]
fn wait_check_producer() {
	let head = AtomicUsize::new(0);
	let tail = AtomicUsize::new(0);
	let closed_count = AtomicUsize::new(0);
	let wc = WakeCheck {
		head: &head,
		tail: &tail,
		closed_count: &closed_count,
		cap: 15,
		is_producer: true,
	};

	assert!(wc.should_wake());
	tail.store(4, Ordering::Release);
	assert!(wc.should_wake());
	tail.store(14, Ordering::Release);
	assert!(!wc.should_wake());
	head.store(4, Ordering::Release);
	assert!(wc.should_wake());

	head.store(5, Ordering::Release);
	tail.store(14, Ordering::Release);
	assert!(wc.should_wake());
	tail.store(0, Ordering::Release);
	assert!(wc.should_wake());
	tail.store(4, Ordering::Release);
	assert!(!wc.should_wake());
}

#[test]
fn wait_check_consumer() {
	let head = AtomicUsize::new(0);
	let tail = AtomicUsize::new(0);
	let closed_count = AtomicUsize::new(0);
	let wc = WakeCheck {
		head: &head,
		tail: &tail,
		closed_count: &closed_count,
		cap: 15,
		is_producer: false,
	};

	assert!(!wc.should_wake());
	tail.store(4, Ordering::Release);
	assert!(wc.should_wake());
	head.store(1, Ordering::Release);
	assert!(wc.should_wake());
	head.store(4, Ordering::Release);
	assert!(!wc.should_wake(), "{:?}", wc);

	head.store(14, Ordering::Release);
	tail.store(3, Ordering::Release);
	assert!(wc.should_wake());
	head.store(0, Ordering::Release);
	assert!(wc.should_wake());
	head.store(3, Ordering::Release);
	assert!(!wc.should_wake());
}

#[test]
fn pop_to_slice() {
	let (mut send, mut recv) = new(15);
	let in_values = [1u8; 10];
	for _ in 0..4 {
		assert_eq!(send.push_slice(&in_values), Ok(10));
		let mut out_values = [0u8; 7];
		assert_eq!(recv.pop_to_slice(&mut out_values), Ok(7));
		assert_eq!(out_values, [1u8; 7]);

		let mut out_values = [0u8; 7];
		assert_eq!(recv.pop_to_slice(&mut out_values), Ok(3));
		assert_eq!(out_values, [1, 1, 1, 0, 0, 0, 0]);
	}
}

/// Assert that the channel endpoints for `!Send` elements are `!Send` themselves.
///
/// ```
/// struct IsSend(*mut ());
/// unsafe impl Send for IsSend {}
/// fn assert_send<T: Send>(_: T) {}
/// let (a,b) = ya_ring_buf::new_waitless::<IsSend>(1);
/// assert_send(a);
/// assert_send(b);
/// ```
///
/// ```compile_fail
/// struct NotSend(*mut ());
/// fn assert_send<T: Send>(_: T) {}
/// let (a,b) = ya_ring_buf::new_waitless::<NotSend>(1);
/// assert_send(a);
/// assert_send(b);
/// ```
#[doc(hidden)]
pub struct _NotSendTest;
