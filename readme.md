Yet Another Ring Buffer
=======================

SPSC channel backed by a ring buffer.

The channel is optimized for sending and receiving bytes - it has several features allowing to produce
and consume items in bulk. This makes it suitable for implementing an intra-process pipe stream.
However it may be used with other types as well.

Features include:

* Lock free, non-blocking push and pop, with no allocation and O(1) time.
* If desired, can integrate blocking, futures-based, or custom waiting. If undesired, no-op waiters add
  no overhead.
* `view` functions for safely accessing the buffer directly, ex. for doing IO directly from
  or to the buffer.
* `no_std` compatible (currently requires `alloc`)

Does not support:

* Multiple producers or consumers. The view API would make this challenging. If you don't mind a
  lack of concurrency, you can wrap the ends in a `Mutex` though.
* Dynamic resizing
